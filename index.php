<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package muxb_theme
 */

get_header(); ?>


<?php if ( have_posts() ) : ?>

<section class="section section-bredcrumbs bg-image-breadcrumbs-1" >
        <div class="shell-fluid context-dark">
          <div class="range range-condensed">
            <div class="cell-xs-10 cell-xl-preffix-1">
              <h1>Classic News</h1>
              <?php if (function_exists('muscle_breadcrumbs')) muscle_breadcrumbs(); ?>
            </div>
          </div>
        </div>
</section>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

	<section class="section section-sm bg-white">

	<div class="shell shell-fluid">	
	<div class="range range-60 range-xl-condensed">

		<div class="cell-md-7 cell-lg-7 cell-xl-6 cell-xl-preffix-1">

			<?php 
			if ( is_home() && ! is_front_page() ) : ?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>

			<?php
			endif;

			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */

				//get_template_part( 'template-parts/content', get_post_format() );
?>
	<section class="section section-sm bg-white">
    <!-- Post Classic-->
    <article class="post post-classic">
    <!-- Post media--><a href="<?php echo get_permalink(); ?>"><?php echo get_the_post_thumbnail( $page->ID, 'full' ); ?></a>
        <!-- Post content-->
        <section class="post-content text-left">
            <h3 class="decorative-title"><a class="link-gray-dark" href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h3>
            <ul class="post-meta list-dotted">
            <li>
                <time datetime="<?php echo get_the_date('c'); ?>" itemprop="datePublished"><?php echo get_the_date(); ?></time>
            </li>

            <li><span class="text-base">by:</span>
            
            <?php printf( '<a class="link link-default" href="%1$s" title="%2$s">%3$s</a>',
                get_author_posts_url( get_the_author_meta( 'ID' ) ),
                sprintf( esc_attr__( 'View all posts by %s', 'atmosphere' ), get_the_author() ),
                ucfirst(get_the_author()) ); ?>

            </li>
            <li>
                <?php the_category(); ?>
            </li>
            <li><a class="link link-default" href="<?php echo get_permalink(); ?>">
            <?php comments_number('0 Comments', '1 Comment', '% Comments') ?>
            </a></li>
            </ul>
            <p class="post-description"><?php the_excerpt(); ?></p>
            <a class="post-link" href="<?php echo get_permalink( $post->ID ); ?>">Read More</a>
        </section>
    </article>
                              
</section>


<?php
			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		
		</div>

		<?php require_once('sidebar.php'); ?>
		
		</div>
		</div>

		</section>
		</main><!-- #main -->
	</div><!-- #primary -->


<?php get_footer(); ?>
