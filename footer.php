<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package muxb_theme
 */
/*
?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="site-info">
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'muxb_theme' ) ); ?>"><?php

				printf( esc_html__( 'Proudly powered by %s', 'muxb_theme' ), 'WordPress' );
			?></a>
			<span class="sep"> | </span>
			<?php

				printf( esc_html__( 'Theme: %1$s by %2$s.', 'muxb_theme' ), 'muxb_theme', '<a href="https://automattic.com/">Automattic</a>' );
			?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->
*/ ?>
<?php wp_footer(); ?>

</body>
</html>
