<?php get_header(); ?>

<section class="section section-bredcrumbs bg-image-breadcrumbs-1">
        <div class="shell-fluid context-dark">
          <div class="range range-condensed">
            <div class="cell-xs-10 cell-xl-preffix-1">
              <h1>Single Post</h1>
              <?php if (function_exists('muscle_breadcrumbs')) muscle_breadcrumbs(); ?>
            </div>
          </div>
        </div>
</section>


  <div id="primary" class="content-area">
    <main id="main" class="site-main">

<section class="section section-sm bg-white">

<div class="shell shell-fluid"> 
<div class="range range-60 range-xl-condensed">

<div class="cell-md-7 cell-lg-7 cell-xl-6 cell-xl-preffix-1">

<?php
  global $more;
  while( have_posts() ) : the_post();
?>

<section class="post-single-body section-lg bg-white">
                <h3 class="decorative-title post-heading"><?php the_title(); ?></h3>
                <ul class="post-meta list-dotted">
                  <li><span class="time"><?php echo get_the_date(); ?></span></li>
                  <li><span class="author">by <?php printf( '<a class="link link-default" href="%1$s" title="%2$s">%3$s</a>',
                    get_author_posts_url( get_the_author_meta( 'ID' ) ),
                    sprintf( esc_attr__( 'View all posts by %s', 'atmosphere' ), get_the_author() ),
                    ucfirst(get_the_author()) ); 
                    ?>
                </span></li>
                <li><?php the_category(); ?></li>
                <li><a class="link link-default" href="<?php echo get_permalink(); ?>">
                <?php comments_number('0 Comments', '1 Comment', '% Comments') ?>
                </a></li>
                </ul><?php echo get_the_post_thumbnail( $page->ID, 'full' ); ?>

                <?php
                
                  $more = 1;
                  the_content();
                endwhile;
                ?>

                <div class="range social-section">
                  <div class="cell-xl-6">
                    <ul class="list-tags">
                      <li><a href="#">Tax</a></li>
                      <li><a href="#">Money</a></li>
                      <li><a href="#">Tips</a></li>
                      <li><a href="#">News</a></li>
                    </ul>
                  </div>
                  <div class="cell-xl-6">
                    <ul class="list-inline list-inline-sm text-xl-right">
                      <li><span>Share This Post!</span></li>
                      <li><a class="icon icon-xxs fa fa-facebook" href="#"></a></li>
                      <li><a class="icon icon-xxs fa fa-twitter" href="#"></a></li>
                      <li><a class="icon icon-xxs fa fa-pinterest" href="#"></a></li>
                      <li><a class="icon icon-xxs fa fa-vimeo" href="#"></a></li>
                      <li><a class="icon icon-xxs fa fa-google" href="#"></a></li>
                      <li><a class="icon icon-xxs fa fa-rss" href="#"></a></li>
                    </ul>
                  </div>
                </div>
 </section>
 <section class="section-lg bg-white">
    <h3 class="decorative-title">Author</h3>
    <article class="blurb-author">
                  <div class="unit unit-spacing-md unit-xs-horizontal">
                    <div class="unit__left">
                        <?php echo get_avatar( get_the_author() ); ?>
                    </div>
                    <div class="unit__body">
                      <h5 class="blurb-author-title">
                    <?php
                        printf( '<a class="link link-default" href="%1$s" title="%2$s">%3$s</a>',
                        get_author_posts_url( get_the_author_meta( 'ID' ) ),
                        sprintf( esc_attr__( 'View all posts by %s', 'atmosphere' ), get_the_author() ),
                        ucfirst(get_the_author()) ); 
                    ?>
                      </h5>
                      <p class="blurb-author-position">Blogger</p>
                      <p class="blurb-author-text">I am a professional blogger interested in everything taking place in cyberspace. I am running this website and try my best to make it a better place to visit. I post only the articles that are related to the topic and thoroughly analyze all visitors’ comments to cater to their needs better.</p>
                    </div>
                  </div>
    </article>
  </section>

   <section class="section-lg bg-white post-single-section text-center text-xs-left">
                <h3 class="decorative-title">Recent Posts</h3>
                
      
      <?php
        $args = array( 'numberposts' => '2' );
        $recent_posts = wp_get_recent_posts( $args );

        $arg = array(
            'post_id' => 1,   // Use post_id, not post_ID
                'count'   => true // Return only the count
        );


        foreach( $recent_posts as $recent ){
        
      ?>    
      <div class="post-project unit unit-spacing-md unit-xs-horizontal">

        <div class="unit__left">
          <a href="<?php echo get_permalink($recent["ID"]) ?>">
            <?php echo get_the_post_thumbnail($recent['ID'], 'medium') ?>
          </a> 
        </div>

        

      <div class="unit__body project-body">
        <div class="project-caption">
          <p class="project-date">
            <span class="project-date"><?php echo get_the_date(); ?></span>
            <span class="project-time">at <?php echo get_the_time(); ?></span>
          </p>
          <h6 class="project-title"><a href="<?php echo get_permalink($recent["ID"])?>"> <?php  echo $recent["post_title"]; ?></a></h6>
        </div>

        <div class="project-meta">
          <span class="comment">
            <a class="link-gray-dark" href="<?php echo get_permalink(); ?>">
              <?php echo get_comments($arg) ?>
            </a>
          </span>
          <span class="author">
            <?php printf( '<a class="link-gray-dark" href="%1$s" title="%2$s">%3$s</a>',
                    get_author_posts_url( get_the_author_meta( 'ID' ) ),
                    sprintf( esc_attr__( 'View all posts by %s', 'atmosphere' ), get_the_author() ),
                    ucfirst(get_the_author()) ); 
            ?>
          </span>
        </div>

      </div>

    </div>

      <?php } ?>

  </section>
              
  <section class="section-lg bg-white">
    <h3 class="decorative-title">Comments</h3>

    <div class="box-comment">



      <div class="unit unit-xs-horizontal unit-spacing-md">

              
              <div class="box-comment">
              <?php wp_list_comments(array('style' => 'div')); ?>
              </div>

<!--         <div class="unit__left">
          <img src="images/box-comment-1-90x90.jpg" alt="" width="90" height="90"/>
        </div>
        <div class="unit__body">
          <ul class="list-inline list-inline-md">
            <li><a href="#">Mildred McCoy</a></li>
            <li class="comment-meta"><span>November 9, 2016,</span> at <span>1:34pm</span>
            </li>
          </ul>
          <p class="comment">The topic you describe here is really important. However, it’s not the only way to save money on tax in the US. I hope you will share some more tips in your future articles.</p>
        </div> -->
      </div>
    
    </div>

     <div class="box-comment box-comment-reply">
<!--       <div class="unit unit-xs-horizontal unit-spacing-md">
        <div class="unit__left"><img src="images/box-comment-2-90x90.jpg" alt="" width="90" height="90"/>
        </div>
        <div class="unit__body">
          <ul class="list-inline list-inline-md">
            <li><a href="#">Kevin Wade</a></li>
            <li class="comment-meta"><span>November 9, 2016,</span> at <span>2:56pm</span>
            </li>
            <li class="author-reply">
              <div class="icon icon-xxs icon-gray fa-reply"></div><span>Mildred McCoy</span>
            </li>
          </ul>
          <p class="comment">Thank you for your comment! Yes, I’m already working on several new posts that will include some important tips and information about US tax regulation and how it can help your bank account.</p>
        </div>
      </div> -->
    </div>

  </section>

              <section class="section-lg bg-white">
                <h3 class="decorative-title">Send a Comment</h3>

<?php comments_template(); ?> 

                <!-- RD Mailform-->
<!--                  -->

              </section>


</div>
            <?php require_once('sidebar.php'); ?>
    </div>
    </div>

    </section>
    </main><!-- #main -->
  </div><!-- #primary -->


<?php get_footer(); ?>