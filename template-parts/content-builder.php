<?php

while ( have_rows('builder') ) : the_row();
	include 'builder/'. get_row_layout() .'.php';
	wp_reset_postdata(); // reset post data for loop
endwhile;