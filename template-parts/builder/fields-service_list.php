<?php while ( have_rows('service_list') ) : the_row(); ?>
	<?php $image = get_sub_field('icon');  ?>
	<div class="cell-sm-4 cell-lg-4">
		<div class="blurb-custom blurb-default">
			<div class="icon icon-image icon-image-lg icon-image-primary">
				<?php wp_get_attachment_image($image['id']); ?>
				<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" width="<?php echo $image['width']; ?>" height="<?php echo $image['height']; ?>"/>
			</div>
			<?php // ToDo rewrite url ?>
			<h5 class="blurb-title"><a href="#"><?php echo get_sub_field('title'); ?></a></h5>
			<p class="blurb-content"><?php echo get_sub_field('description'); ?></p>
		</div>
	</div>
<?php endwhile; ?>