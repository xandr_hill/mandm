<?php if ( get_row_layout() == 'teams' ): ?>
	<!--Our Management Team-->
	<section class="section section-sm bg-white">
		<div class="shell text-center">
			<div class="range range-40 range-md-60 range-center">
				<div class="cell-sm-12">
					<div class="range range-center">
						<div class="cell-sm-10 cell-lg-8">
							<h3 class="decorative-title"><?php echo get_sub_field( 'title' ); ?></h3>
							<p><?php echo get_sub_field( 'subtitle' ); ?></p>
						</div>
					</div>
				</div>
				<?php if ( get_sub_field('teams') ) : ?>
					<?php
                       $args = array(
                            'post_type' => 'team',
                            'post__in' => get_sub_field('teams'),
                            'orderby'   => 'post__in',
                            'order'     => 'ASC',
						);

						$teams = new WP_Query( $args );
					?>

					<?php if( $teams->have_posts() ): ?>
						<?php while ($teams->have_posts() ) : $teams->the_post(); ?>
							<div class="cell-sm-4 cell-md-4">
								<div class="thumbnail-type-1 text-center">
									<figure>
                                        <?php echo get_the_post_thumbnail(); ?>
									</figure>
									<div class="caption">
										<h5 class="decorative-title caption-title"><a href="#"><?php the_title(); ?></a></h5>
										<p class="caption-subtitle"><?php the_field('position'); ?></p>

                                        <ul class="list-inline list-inline-sm">
                                            <?php while ( have_rows('social_networks') ) : the_row(); ?>
                                                <li><a class="link-default icon icon-xss fa <?php the_sub_field('icon'); ?>" href="<?php the_sub_field('link'); ?>"></a></li>
                                            <?php endwhile; ?>
										</ul>
									</div>
								</div>
							</div>
						<?php endwhile; ?>
					<?php endif; ?>

				<?php endif; wp_reset_query(); ?>

			</div>
		</div>
	</section>
<?php endif; ?>