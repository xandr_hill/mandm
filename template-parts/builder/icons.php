<?php if ( get_row_layout() == 'icons' ): ?>
	<!-- RD Parallax-->
	<section class="section rd-parallax">
	<?php while ( have_rows( 'numbers' ) ) : the_row(); ?>
		<div class="rd-parallax-layer" data-speed="0.2" data-type="media" data-url="<?php echo get_sub_field('background'); ?>"></div>
	<?php endwhile; ?>
		<div class="rd-parallax-layer section-sm context-dark" data-speed="0" data-type="html">
			<?php while ( have_rows( 'numbers' ) ) : the_row(); ?>
				<?php include 'fields-numbers.php'; ?>
			<?php endwhile; ?>
		</div>
	</section>
<?php endif; ?>