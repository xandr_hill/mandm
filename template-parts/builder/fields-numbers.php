<div class="shell text-center">
	<div class="range range-40">
		<?php while ( have_rows('icons') ) : the_row(); ?>
			<div class="cell-sm-6 cell-lg-3">
				<div class="counter-wrap counter-icon">
					<div class="icon icon-md icon-white mdi <?php echo get_sub_field('icon'); ?>"></div>
					<div class="heading-1"><span class="counter" data-step="100000"><?php echo get_sub_field('description'); ?></span></div>
					<h6 class="counter-description"><?php echo get_sub_field('text'); ?></h6>
				</div>
			</div>
		<?php endwhile; ?>
	</div>
</div>