<?php if ( get_row_layout() == 'double_block' ): ?>
	<!-- Latest Company News-->
	<section class="section section-sm bg-gray">
		<div class="shell text-center text-xs-left">
			<div class="range range-60">
				<div class="cell-sm-12 text-center">
					<h3><?php echo get_sub_field('title'); ?></h3>
				</div>
					<?php $query = new WP_Query( array( 'posts_per_page' => get_sub_field('count') ) );  ?>
						<?php if( $query->have_posts() ): ?>
							<?php while ($query->have_posts() ) : $query->the_post(); ?>
								<div class="cell-md-6">
									<div class="post-project unit unit-spacing-md unit-xs-horizontal">
										<div class="unit__left"><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_post_thumbnail(); ?></a></div>
										<div class="unit__body project-body">
											<div class="project-caption">
												<p class="project-date"><span class="project-date"><?php the_time('F d, Y'); ?></span> <span class="project-time">at <?php the_time(); ?></span>
												</p>
												<h6 class="project-title"><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_excerpt(); ?></a></h6>
											</div>
											<div class="project-meta"><span class="comment"><a class="link-gray-dark" href="<?php echo get_the_permalink(); ?>"> <?php comments_number( 0, 1, '%' ); ?> </a></span><span class="author"><a class="link-gray-dark" href="<?php echo get_the_permalink(); ?>"><?php the_author_meta( 'display_name' ); ?></a></span></div>
										</div>
									</div>
								</div>
							<?php endwhile; ?>
						<?php endif; wp_reset_query(); ?>

						<div class="cell-sm-12 text-center"><a class="button button-primary" href="#"><?php echo get_sub_field('link_text'); ?></a></div>
					</div>
			</div>
	</section>
<?php endif; ?>