<?php if ( get_row_layout() == 'services' ): ?>
    <!-- Areas of Work-->
    <section class="section section-sm">
        <div class="shell text-center">
            <div class="range range-40 range-md-60">
                <div class="cell-sm-12">
                    <div class="range range-center">
                        <div class="cell-sm-10 cell-lg-6">
                            <h3><?php echo get_sub_field( 'title' ); ?></h3>
                            <p><?php echo get_sub_field( 'subtitle' ); ?></p>
                        </div>
                    </div>
                </div>
				<?php while ( have_rows( 'services' ) ) : the_row(); ?>
					<?php include 'fields-service_list.php'; ?>
				<?php endwhile; ?>
            </div>
        </div>
    </section>
<?php endif; ?>