<?php if( get_row_layout() == 'slider' ): ?>
	<?php while ( have_rows('slides') ) : the_row(); ?>
        <section class="section">
            <!-- Swiper-->
            <div class="swiper-container swiper-slider swiper-slider-custom" data-autoplay="false" data-simulate-touch="false">
                <div class="swiper-wrapper">
                    <?php include 'fields-slider.php'; ?>
                </div>
                <!-- Swiper pagination-->
                <div class="swiper-pagination"></div>
                <!-- Swiper Navigation-->
                <div class="swiper-button swiper-button-prev"><span class="swiper-button__arrow"><span class="fa fa-angle-left"></span></span>
                    <div class="preview">
                        <h3 class="title">Text</h3>
                        <div class="preview__img"></div>
                    </div>
                </div>
                <div class="swiper-button swiper-button-next"><span class="swiper-button__arrow"><span class="fa fa-angle-right"></span></span>
                    <div class="preview">
                        <h3 class="title">Text</h3>
                        <div class="preview__img"></div>
                    </div>
                </div>
            </div>
        </section>
	<?php endwhile; ?>
<?php endif; ?>