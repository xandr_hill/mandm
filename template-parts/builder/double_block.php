<?php if ( get_row_layout() == 'double_block' ): ?>
	<?php if( have_rows('blocks') ) : ?>
		<section class="section section-sm bg-gray">
			<div class="shell">
				<div class="range range-40">
					<?php while ( have_rows('blocks') ) : the_row();?>
						<?php if ( get_row_layout() == 'video' ): ?>
							<div class="cell-md-6 cell-lg-5">
								<?php $image = get_sub_field('video_image'); ?>
								<a class="mfp-link video-cover" style="background-image: url(<?php echo $image['url'] ?>);" data-lightbox="iframe" href="<?php echo get_sub_field('video_url') ?>"><span class="icon icon-xl icon-white mdi mdi-play-circle-outline"></span></a>
							</div>
						<?php endif; ?>
						<?php if ( get_row_layout() == 'text_block' ): ?>
							<div class="cell-md-6 cell-lg-preffix-1">
								<?php echo get_sub_field('text_block') ?>
							</div>
						<?php endif; ?>
					<?php endwhile; ?>
				</div>
			</div>
		</section>
	<?php endif; ?>
<?php endif; ?>