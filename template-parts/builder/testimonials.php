<?php if ( get_row_layout() == 'testimonials' ): ?>
	<!-- RD Parallax-->
	<section class="section rd-parallax">
		<div class="rd-parallax-layer" data-speed="0.2" data-type="media" data-url="<?php echo get_sub_field( 'background' ); ?>"></div>
		<div class="rd-parallax-layer section-sm context-dark" data-speed="0" data-type="html">
			<div class="shell">
				<div class="range range-40">
					<div class="cell-sm-12 text-center">
						<h3><?php echo get_sub_field( 'title' ); ?></h3>
					</div>

					<?php if ( get_sub_field('testimonials') ) : ?>
						<?php
//						var_dump(get_sub_field('testimonials'));
						$args = array(
							'post_type' => 'testimonials',
							'post__in' => get_sub_field('testimonials'),
							'orderby'   => 'post__in',
							'order'     => 'ASC',
						);

						$testimonials = new WP_Query( $args );
						?>
						<?php if( $testimonials->have_posts() ): ?>
							<div class="cell-sm-12">
								<!-- Owl Carousel-->
								<div class="owl-carousel owl-carousel-var-2" data-items="1" data-sm-items="3" data-lg-items="3" data-xl-items="3" data-dots="true" data-nav="false" data-stage-padding="15" data-loop="false" data-margin="30" data-mouse-drag="false">
									<?php while ($testimonials->have_posts() ) : $testimonials->the_post(); ?>
										<div class="owl-item">
										<blockquote class="quote quote-custom quote-secondary">
											<div class="quote-meta">
												<div class="unit unit-horizontal unit-spacing-xxs unit-middle">
													<div class="unit__left"><?php echo get_the_post_thumbnail($post->ID, 'thumbnail', array( 'class' => 'img-circle' )); ?>
													</div>
													<div class="unit__body">
														<cite><?php the_title(); ?></cite><small><span><?php the_field('position'); ?></span></small>
													</div>
												</div>
											</div>
											<div class="quote-body">
												<p>
													<q><?php the_field('description'); ?></q>
												</p>
											</div>
										</blockquote>
									</div>
									<?php endwhile; ?>
								</div>
							</div>
						<?php endif; wp_reset_query(); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>