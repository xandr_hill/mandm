<?php while ( have_rows('slider') ) : the_row(); ?>
	<?php $image = get_sub_field('background_image'); ?>
	<div class="swiper-slide" data-slide-bg="<?php echo $image['url']; ?>" data-slide-title="<?php echo $image['title']; ?>">
		<div class="swiper-slide-caption">
			<div class="shell">
				<div class="range range-center range-lg-left">
					<div class="cell-sm-10 cell-md-10 cell-xl-9">
						<div class="box-content" data-caption-animate="fadeInUp" data-caption-delay="100">
							<h1><?php echo get_sub_field('title'); ?></h1>
							<h3><?php echo get_sub_field('subtitle'); ?></h3>
							<div class="divider divider-4"></div>
							<?php echo get_sub_field('description'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endwhile; ?>