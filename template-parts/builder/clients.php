<?php if ( get_row_layout() == 'clients' ): ?>
	<!-- RD Parallax-->
	<section class="section section-xs bg-gray">
		<div class="shell text-center">
			<!-- Owl Carousel-->

					<?php if ( get_sub_field('clients') ) : ?>
						<?php
						$args = array(
							'post_type' => 'clients',
							'post__in' => get_sub_field('clients'),
							'orderby'   => 'post__in',
							'order'     => 'ASC',
						);

						$clients = new WP_Query( $args );
						?>
						<?php if( $clients->have_posts() ): ?>
							<div class="cell-sm-12">
								<!-- Owl Carousel-->
								<div class="owl-carousel owl-carousel-var-2" data-items="1" data-sm-items="5" data-md-items="5" data-dots="true" data-nav="false" data-stage-padding="15" data-loop="false" data-margin="30" data-mouse-drag="false">
									<?php while ($clients->have_posts() ) : $clients->the_post(); ?>
										<div class="owl-item"><a class="link-hover-1" href="#"><?php echo get_the_post_thumbnail(); ?></a></div>
									<?php endwhile; ?>
								</div>
							</div>
						<?php endif; wp_reset_query(); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>