

    <div class="cell-md-5 cell-lg-4 cell-xl-3 cell-lg-preffix-1">

      <!-- Section Blog Modern-->
              <aside class="text-left">
                <div class="range range-60 range-md-90">
                  <div class="cell-sm-12 cell-md-12">
                    <h5 class="decorative-title">Search</h5>
                    <!-- Search Form-->
                                  <!-- RD Search Form-->
                                  <form class="form-search rd-search rd-mailform-inline rd-mailform-small" action="search-results.html" method="GET">
                                    <div class="form-wrap">
                                      <label class="form-label form-search-label form-label-sm" for="blog-sidebar-2-form-search-widget">Enter a keyword</label>
                                      <input class="form-input form-search-input form-control #{inputClass}" id="blog-sidebar-2-form-search-widget" type="text" name="s" autocomplete="off">
                                    </div>
                                    <button class="button button-sm button-primary form-search-submit" type="submit">Search</button>
                                  </form>
                  </div>

                  <div class="cell-sm-6 cell-md-12">
                    <h5 class="decorative-title">Archive</h5>
                    <ul class="list-marked list-marked-1">
                      <?php wp_get_archives(); ?>
                    </ul>
                  </div>

                  <?php if ( get_post_gallery() ) : ?>



                  <div class="cell-sm-6 cell-md-12">
                    <h5 class="decorative-title">Gallery</h5>
                    <ul class="gallery-custom" data-photo-swipe-gallery="gallery">
                             
                    <?php 
				        $gallery = get_post_gallery_images();
				        foreach ($gallery as $image) {
				            // Loop through each image in each gallery
				            $image_list .= '<li><a class="thumbnail-classic" rel="prettyPhoto[gal]" href=" ' . str_replace('-150x150', '', $image) . ' "><div class="thumbnail-overlay"><img src="' . str_replace('-150x150', '', $image) . '"  /></div></li></a>';
				        }
				        echo $image_list;
        			?>
                                 
                    </ul>
                  </div>
				<?php endif; ?>

                  <div class="cell-sm-6 cell-md-12">
                    <h5 class="decorative-title">Categories</h5>
                    <ul class="list-marked list-marked-1">
                      <li><?php wp_list_categories('style=none'); ?></li>
                    </ul>
                  </div>
                  <div class="cell-sm-6 cell-md-12">
                    <h5 class="decorative-title">About</h5>
                    <div class="sidebar-description">
                      <p>We are dedicated to providing professional service with the highest degree of honesty and integrity, and strive to solve your tax issues.</p><a class="post-link" href="http://muscleandmotion.mylights.pro">Read More</a>
                    </div>
                  </div>

                </div>
              </aside>
    
    </div>

