<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package muxb_theme
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<header id="masthead" class="site-header">
        <nav id="site-navigation" class="main-navigation rd-navbar rd-navbar-default rd-navbar-default-top-panel" data-sm-stick-up-offset="1px" data-md-stick-up-offset="44px" data-lg-stick-up-offset="70px" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fullwidth" data-md-device-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-sm-stick-up="true" data-md-stick-up="true" data-lg-stick-up="true">
            <div class="rd-navbar-outer outer-2">
                <div class="rd-navbar-inner">
                <!-- RD Navbar Panel-->
                <div class="rd-navbar-cell rd-navbar-panel">
                    <!-- RD Navbar Toggle-->
                    <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav"><span class="toggle-icon"></span></button>
                    <!-- RD Navbar Brand--><a class="rd-navbar-brand" href="./"><img class="brand-mini" src="images/brand.png" width="180" height="33" alt=""></a>
                </div>
                <?php
                    wp_nav_menu( array(
                        'theme_location' => 'menu-1',
                        'container_class' => 'rd-navbar-cell rd-navbar-nav-wrap',
                        'menu_class'     => 'rd-navbar-nav',
                        'menu_id'        => 'primary-menu',
                    ) );
                ?>
            </div>
            </div>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
