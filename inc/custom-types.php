<?php

// Register Custom Post Type
function custom_post_types() {

	$clients = array(
		'labels'                => array(
			'name'                  => _x( 'Clients', 'basics_theme' ),
			'singular_name'         => _x( 'Clients', 'basics_theme' ),
			'menu_name'             => __( 'Clients', 'basics_theme' ),
			'name_admin_bar'        => __( 'Clients', 'basics_theme' )
		),

		'supports' => array( 'title', 'thumbnail'),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'menu_icon' => 'dashicons-schedule',
	);
	register_post_type( 'clients', $clients );

	$team = array(
		'labels'                => array(
			'name'                  => _x( 'Team', 'basics_theme' ),
			'singular_name'         => _x( 'Team', 'basics_theme' ),
			'menu_name'             => __( 'Team', 'basics_theme' ),
			'name_admin_bar'        => __( 'Team', 'basics_theme' )
		),

		'supports' => array( 'title', 'thumbnail'),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'menu_icon' => 'dashicons-schedule',
	);
	register_post_type( 'team', $team );

	$testimonials = array(
		'labels'                => array(
			'name'                  => _x( 'Testimonials', 'basics_theme' ),
			'singular_name'         => _x( 'Testimonials', 'basics_theme' ),
			'menu_name'             => __( 'Testimonials', 'basics_theme' ),
			'name_admin_bar'        => __( 'Testimonials', 'basics_theme' )
		),

		'supports' => array( 'title', 'thumbnail'),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'menu_icon' => 'dashicons-schedule',
	);
	register_post_type( 'testimonials', $testimonials );

//	$board_args = array(
//		'labels'                => array(
//			'name'                  => _x( 'Board', 'basics_theme' ),
//			'singular_name'         => _x( 'Board', 'basics_theme' ),
//			'menu_name'             => __( 'Board', 'basics_theme' ),
//			'name_admin_bar'        => __( 'Board', 'basics_theme' )
//		),
//
//		'supports' => array( 'title', 'thumbnail', 'excerpt', 'editor','page-attributes'),
//		'hierarchical'          => false,
//		'public'                => true,
//		'show_ui'               => true,
//		'show_in_menu'          => true,
//		'menu_position'         => 5,
//		'show_in_admin_bar'     => true,
//		'show_in_nav_menus'     => true,
//		'can_export'            => true,
//		'has_archive'           => false,
//		'exclude_from_search'   => false,
//		'publicly_queryable'    => true,
//		'capability_type'       => 'post',
//		'menu_icon' => 'dashicons-schedule',
//	);
//	register_post_type( 'board', $board_args );

//	$customers_args = array(
//		'labels'                => array(
//			'name'                  => _x( 'Customers', 'basics_theme' ),
//			'singular_name'         => _x( 'Customers', 'basics_theme' ),
//			'menu_name'             => __( 'Customers', 'basics_theme' ),
//			'name_admin_bar'        => __( 'Customers', 'basics_theme' )
//		),
//
//		'supports' => array( 'title', 'thumbnail'),
//		'hierarchical'          => false,
//		'public'                => true,
//		'show_ui'               => true,
//		'show_in_menu'          => true,
//		'menu_position'         => 5,
//		'show_in_admin_bar'     => true,
//		'show_in_nav_menus'     => true,
//		'can_export'            => true,
//		'has_archive'           => false,
//		'exclude_from_search'   => false,
//		'publicly_queryable'    => true,
//		'capability_type'       => 'post',
//		'menu_icon' => 'dashicons-schedule',
//	);
//	register_post_type( 'customers', $customers_args );
//
//	$news_args = array(
//		'labels'                => array(
//			'name'                  => _x( 'News', 'basics_theme' ),
//			'singular_name'         => _x( 'News', 'basics_theme' ),
//			'menu_name'             => __( 'News', 'basics_theme' ),
//			'name_admin_bar'        => __( 'News', 'basics_theme' )
//		),
//
//		'supports' => array( 'title', 'thumbnail', 'excerpt', 'editor'),
//		'hierarchical'          => false,
//		'public'                => true,
//		'show_ui'               => true,
//		'show_in_menu'          => true,
//		'menu_position'         => 5,
//		'show_in_admin_bar'     => true,
//		'show_in_nav_menus'     => true,
//		'can_export'            => true,
//		'has_archive'           => false,
//		'exclude_from_search'   => false,
//		'publicly_queryable'    => true,
//		'capability_type'       => 'post',
//		'menu_icon' => 'dashicons-schedule',
//	);
//	register_post_type( 'news', $news_args );
//
//	$events_args = array(
//		'labels'                => array(
//			'name'                  => _x( 'Events', 'basics_theme' ),
//			'singular_name'         => _x( 'Events', 'basics_theme' ),
//			'menu_name'             => __( 'Events', 'basics_theme' ),
//			'name_admin_bar'        => __( 'Events', 'basics_theme' )
//		),
//
//		'supports' => array( 'title', 'thumbnail', 'excerpt', 'editor'),
//		'hierarchical'          => false,
//		'public'                => true,
//		'show_ui'               => true,
//		'show_in_menu'          => true,
//		'menu_position'         => 5,
//		'show_in_admin_bar'     => true,
//		'show_in_nav_menus'     => true,
//		'can_export'            => true,
//		'has_archive'           => false,
//		'exclude_from_search'   => false,
//		'publicly_queryable'    => true,
//		'capability_type'       => 'post',
//		'menu_icon' => 'dashicons-schedule',
//	);
//	register_post_type( 'events', $events_args );
//
//	$video_args = array(
//		'labels'                => array(
//			'name'                  => _x( 'Video', 'basics_theme' ),
//			'singular_name'         => _x( 'Video', 'basics_theme' ),
//			'menu_name'             => __( 'Video', 'basics_theme' ),
//			'name_admin_bar'        => __( 'Video', 'basics_theme' )
//		),
//
//		'supports' => array( 'title', 'thumbnail', 'excerpt', 'editor'),
//		'hierarchical'          => false,
//		'public'                => true,
//		'show_ui'               => true,
//		'show_in_menu'          => true,
//		'menu_position'         => 5,
//		'show_in_admin_bar'     => true,
//		'show_in_nav_menus'     => true,
//		'can_export'            => true,
//		'has_archive'           => false,
//		'exclude_from_search'   => false,
//		'publicly_queryable'    => true,
//		'capability_type'       => 'post',
//		'menu_icon' => 'dashicons-schedule',
//	);
//	register_post_type( 'video', $video_args );
}
add_action( 'init', 'custom_post_types', 0 );
